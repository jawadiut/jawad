package model;

/**
 * Created by jawad on 1/30/16.
 */
public class LicenseInfo {
    private String serialNo;
    private String model;
    private String feature;
    private String authCode;
    private String issueDate;
    private String expiry;
    private String actions;

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public String toString() {
        return "\nSerial No: " + this.serialNo
                + "\nModel: " + this.model
                + "\nFeature: " + this.feature
                + "\nAuthcode: " + this.authCode
                + "\nIssue Date: " + this.issueDate
                + "\nExpirty: " + this.expiry
                + "\nActions: " + this.actions;
    }
}
