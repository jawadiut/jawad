package service;

import model.LicenseInfo;

import javax.security.auth.login.LoginException;
import java.util.List;

/**
 * Created by jawad on 1/30/16.
 */
public interface LicenseInfoExtractor {

    List<LicenseInfo> login(String url, String userId, String password) throws LoginException;
}
