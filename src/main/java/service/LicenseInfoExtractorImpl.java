package service;

import com.jaunt.*;
import com.jaunt.component.Form;
import com.jaunt.component.Table;
import model.LicenseInfo;
import org.apache.log4j.Logger;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jawad on 1/30/16.
 */
public class LicenseInfoExtractorImpl implements LicenseInfoExtractor {

    public static final Logger logger = Logger.getLogger(LicenseInfoExtractorImpl.class);

    public List<LicenseInfo> login(String url, String userId, String password) throws LoginException {

        List<LicenseInfo> licenseInfoList = new ArrayList<LicenseInfo>();
        String loginUrl = "https://fed-stage.juniper.net/auth/xlogin.html";
        UserAgent userAgent = new UserAgent();

        try {
            userAgent.visit(url);
            userAgent.sendPOST(url, "username=" + userId + "&password=" + password);

            if (userAgent.getLocation().equals(loginUrl)) {
                throw new LoginException("User is not Authorized");
            }

            Form form = userAgent.doc.getForm("<form name=\"SearchLicenseFormBean\"");
            userAgent.sendPOST(form.getAction(), "methodToCall=search"
                    + "&searchCriteria=SEARCH_BY_SERIAL_NO"
                    + "&searchText=ANKRTU-10");

            Element element = userAgent.doc.findFirst("<table class=\"dataTable\">");
            Elements elements = element.findEach("<tr>");
            Table table = userAgent.doc.getTable(element);
            String serialNo = table.getCol(0).getElement(1).getText();
            String model = table.getCol(1).getElement(1).getText();

            for (int row = 1; row < elements.size(); row++) {
                LicenseInfo licenseInfo = new LicenseInfo();
                for (int col = 0; col < table.getRow(row).size(); col++) {

                    Element rowElement = table.getCol(col).getElement(row);
                    String elementText = (col == 0) ? serialNo
                            : (col == 1) ? model
                            : rowElement.getChildElements().isEmpty() ? rowElement.getText()
                            : rowElement.getElement(0).getText() + rowElement.getText();

                    addLicenseInfo(licenseInfo, col, elementText);
                }
                licenseInfoList.add(licenseInfo);
            }

            int count = 1;
            for (LicenseInfo licenseInfo : licenseInfoList) {
                logger.info("License Info #" + (count++) + ": " + licenseInfo.toString());
            }


            return licenseInfoList;

        } catch (ResponseException e) {
            throw new LoginException("Something went wrong");
        } catch (NotFound e) {
            throw new LoginException("Something went wrong");
        }
    }

    private LicenseInfo addLicenseInfo(LicenseInfo licenseInfo, int columnNo, String text) {
        if (columnNo == 0) {
            licenseInfo.setSerialNo(text);
        } else if (columnNo == 1) {
            licenseInfo.setModel(text);
        } else if (columnNo == 2) {
            licenseInfo.setFeature(text);
        } else if (columnNo == 3) {
            licenseInfo.setAuthCode(text);
        } else if (columnNo == 4) {
            licenseInfo.setIssueDate(text);
        } else if (columnNo == 5) {
            licenseInfo.setExpiry(text);
        } else {
            licenseInfo.setActions(text);
        }

        return licenseInfo;
    }

    public static void main(String[] args) {
        LicenseInfoExtractor licenseInfoExtractor = new LicenseInfoExtractorImpl();
        try {
            final List<LicenseInfo> juniper = licenseInfoExtractor.login("http://stage.juniper.net/lcrs", "jarrodev@gmail.com", "juniper");
            logger.info("total size: " + juniper.size());

        } catch (LoginException e) {
            logger.error("Unable to login", e);
        }
    }
}
